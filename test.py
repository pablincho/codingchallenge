import csv
from main import weight_row, cast_to_int

FIELD_NAMES = ['person_id', 'name', 'last_name', 'current_role', 'country', 'industry', 'num_of_rec', 'num_of_con']


def test_output():
    with open('people.out', 'r') as file:
        reader = csv.reader(file)
        for idx, row in enumerate(reader):
            print(idx + 1, row)


def test_row_weight():
    row1 = {'last_name': 'ibrahim', 'name': 'ramy s.', 'country': 'Saudi Arabia', 'industry': 'Manufacturing',
            'num_of_rec': '0', 'person_id': '643781637', 'num_of_con': '0',
            'current_role': 'senior structural engineer'}

    weight = weight_row(row1)
    assert weight == 20

    row2 = {'last_name': 'brandts', 'name': 'maurice', 'country': 'Netherlands', 'industry': 'Photography',
            'num_of_rec': '0', 'person_id': '644373356', 'num_of_con': '69', 'current_role': ''}

    weight = weight_row(row2)
    assert weight == 69

    row3 = {'last_name': 'soares', 'name': 'ademar marcelo', 'country': 'Brazil', 'industry': 'Hospital & Health Care',
            'num_of_rec': '1', 'person_id': '644349490', 'num_of_con': '500', 'current_role': ''}
    weight = weight_row(row3)
    assert weight == 501

    row4 = {'num_of_con': '3'}
    int1 = cast_to_int(row4, 'num_of_con')
    assert int1 == 3

    row5 = {'num_of_con': 'string'}
    int2 = cast_to_int(row5, 'num_of_con')
    assert int2 == 0


if __name__ == '__main__':
    test_row_weight()
