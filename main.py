import csv

FIELD_NAMES = ['person_id', 'name', 'last_name', 'current_role', 'country', 'industry', 'num_of_rec', 'num_of_con']
KEY_ROLE_WORDS = ['director', 'chairman', 'manager', 'management', 'owner', 'president', 'senior', 'supervisor']
KEY_INDUSTRY_ROLES = ['technology', 'manufacturing', 'electronics', 'banking', 'energy', 'mining']


def suggest_clients():
    output_list = []
    with open('people.in') as file:
        reader = csv.DictReader(file, fieldnames=FIELD_NAMES, delimiter='|')
        for row in reader:
            output_list.append(row)

    sorted_list = sorted(output_list, key=lambda i: weight_row(i), reverse=True)

    with open('people.out', 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerows(((i.get('person_id'),) for i in sorted_list[:100]))


def weight_row(row):
    num_of_con = cast_to_int(row, 'num_of_con')
    num_of_rec = cast_to_int(row, 'num_of_rec')

    weight = num_of_rec + num_of_con

    weight += weight_field(row, 'current_role', KEY_ROLE_WORDS)
    weight += weight_field(row, 'industry', KEY_INDUSTRY_ROLES)

    return weight


def cast_to_int(row, field):
    string_field = row.get(field)
    if string_field:
        try:
            int_field = int(string_field)
        except ValueError:
            int_field = 0
    else:
        int_field = 0
    return int_field


def weight_field(row, field, key_words):
    current_field = row.get(field)
    field_weight = 0
    if current_field:
        for word in key_words:
            if word in current_field.lower():
                field_weight = 10
                break
    return field_weight


if __name__ == '__main__':
    suggest_clients()
